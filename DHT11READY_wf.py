#!/usr/bin/python
import Adafruit_DHT
import json
from time import strftime, gmtime, sleep
from shutil import copyfile
tempfile="temp_hum_w.json"
filename="temp_hum.json"
count=0
#fw=open(filename,'w')
blckd=[]
while True:
	#print count
	humidity, temperature = Adafruit_DHT.read_retry(11, 6)  # GPIO27 (BCM notation)
    	#print ("Humidity = {} %; Temperature = {} C".format(humidity, temperature))
	time=strftime("%Y-%d-%m %H:%M:%S", gmtime())
	hum="{}".format(humidity);temp="{}".format(temperature)
	#print hum,temp
	blckd.append({'time':str(time),'humidity':hum,'temperature':temp})
	#b=("Humidity : {} , Temperature : {} ".format(humidity,temperature))
	#a=a+"{"\'"+time\' : "+"\'"+str(time)+"\'"+","+b+"},"
	if(count%10==0):
		#count=count+1
		#print count,"mod"
		fw=open(tempfile,'w')
		fw.write(json.dumps(blckd))
		fw.close()
		sleep(2)
		copyfile(tempfile,filename)
		sleep(2)
		blckd=[]
	count=count+1
