/**
 * IotController
 *
 * @description :: Server-side logic for managing Iots
 * @help        :: See http://sailsjs.org/#!/documentation/concepts/Controllers
 */
var exec = require('child_process').exec;
//var json1 = require('/var/www/sail_project/temp_hum.json');
function child(command){
 exec(command,
    function (error, stdout, stderr) {
        console.log('stdout: ' + stdout);
        console.log('stderr: ' + stderr);
        if (error !== null) {
             console.log('exec error: ' + error);
        }
    });
};

module.exports = {
 switch : function(req,res){
  led=req.param('led','none');
  status=req.param('status','off');
  timeon=req.param('time','0');
  //child("pwd >pwd.txt");
  child("/var/www/sail_project/api/controllers/control_led.py"+" "+led+" "+status+" "+timeon);
  return res.send("status"+req.param("status"));
 }, 	
 gettemph : function(req,res){
  //var json = require('./temp_hum.json'); //with path
 var fs = require('fs');
 var rep1;
 fs.readFile( 'temp_hum.json', function (err, data) {
 if (err) {
    //throw err; 
 }
 //console.log(data.toString());
 rep1=data.toString();
 return res.send(JSON.parse(data));
 });
 //return res.send(rep1);
 }

};

