#!/usr/bin/python

import sys
from gpiozero import LED
from time import sleep
#print 'Number of arguments:', len(sys.argv), 'arguments.'
#print 'Argument List:', str(sys.argv)
#print str(sys.argv[1])+" other arg "+str(sys.argv[2])
#print eval(sys.argv[1])
try :
	number=int(eval(sys.argv[1]))
except Exception:
	quit()
if not( 3 < number < 24):
	quit() 
led=LED(number)
try:
	state=sys.argv[2]
except Exception:
	state="off"
if state=="on":
	if led.is_active:
		quit()
	led.on()
else:
	led.off()
	quit()
try:
	timeout=eval(sys.argv[3])
except Exception:
	timeout=0
sleep(timeout)
led.off()
print "finish led: "+str(number)+" state: "+str(state)+" timeout: "+str(timeout)
quit()
