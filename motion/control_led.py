#!/usr/bin/python

import sys
import os
import signal
from gpiozero import LED
from time import sleep
pidfile="./pidfile"#to store pid of this  process
filename="./ledrunning" #to indicate that the led is on or off
swe="./scr_w_exe"#script well executed from begin to end


if os.path.isfile(pidfile):
	file_pid = open(pidfile,'r')
	last_pid = int( file_pid.readline() )
	file_pid.close()
#------------------------------------------------------------
file_pid=open(pidfile,'w')
file_pid.write( str(  os.getpid() ) )
file_pid.close()
#------------------------------------------------------------
if os.path.isfile(swe):os.remove(swe)
print "starting"
try :
	number=int(eval(sys.argv[1]))
except Exception:
	quit()
if not( 3 < number < 24):
	if os.path.isfile(filename):os.remove(filename)
	open(swe,'w').close()
	quit() 
try:
        state=sys.argv[2]
except Exception:
        state="off"
if state=="on":
        try:
                if os.path.isfile(filename) and not(os.path.isfile(swe)) : print "is currently active" ; quit()
        except Exception:
                pass
led=LED(number)
if state=="on":
	led.on()
	open(filename,'w').close()
else:
	if os.path.isfile(filename):os.remove(filename)
	led.off()
	open(swe,'w').close()
	try:
		os.kill(lastpid,signal.SIGKILL)
		os.remove(filename)
	except Exception:
		pass	
	quit()
try:
	timeout=eval(sys.argv[3])
except Exception:
	timeout=0
if state=="off":
	timeout=0
sleep(timeout)
led.off()
print "finish led: "+str(number)+" state: "+str(state)+" timeout: "+str(timeout)
if os.path.isfile(filename):os.remove(filename)
open(swe,'w').close()
quit()
